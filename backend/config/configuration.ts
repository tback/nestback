export default () => ({
  initial_health_points: parseInt(process.env.INITIAL_HEALTH_POINTS, 10) || 10,
  click_damage: parseInt(process.env.CLICK_DAMAGE, 10) || 1,
  redis_url: process.env.REDIS_URL || 'redis://:@localhost:6379/0'
})