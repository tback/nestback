import { Body, ClassSerializerInterceptor, Controller, Get, Post, UseInterceptors } from '@nestjs/common';
import * as os from 'os';
import { Stats } from './stats';
import { BackendService } from './backend.service';

export class CreateBackend {
  name: string;
}

@Controller()
@UseInterceptors(ClassSerializerInterceptor)
export class AppController {
  constructor(
    private backendService: BackendService,
  ) {
  }

  @Get()
  async getBackends(): Promise<Stats> {
    const hostname = os.hostname();
    const stats = new Stats();
    stats.backends = await this.backendService.getBackends();
    return stats;
  }

  @Post()
  async shoot(@Body() backend: CreateBackend): Promise<Stats> {
    console.log(backend.name);
    if (backend.name == os.hostname()){
      await this.backendService.shootBackend(backend.name);
    }
    const stats = new Stats();
    return stats;
  }
}
