import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RedisModule } from 'nestjs-redis';
import { LoggerMiddleware } from './logger.middleware';
import { ConfigModule, ConfigService } from '@nestjs/config';
import configuration from '../config/configuration';
import { BackendService } from './backend.service';
import { TerminusModule } from '@nestjs/terminus';
import { HealthController } from './health/health.controller';
import { BackendHealthIndicator } from './health/backend.health';


@Module({
  imports: [
    // ConfigModule.load(path.resolve(__dirname, 'config', '**/!(*.d).{ts,js}')),
    ConfigModule.forRoot({ load: [configuration] }),
    RedisModule.register({ url: process.env.REDIS_URL }),
    TerminusModule,
  ],
  controllers: [AppController, HealthController],
  providers: [AppService, BackendService, BackendHealthIndicator],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): any {
    consumer
      .apply(LoggerMiddleware)
      .forRoutes({ path: '*', method: RequestMethod.ALL });
  }

}
