import { Injectable } from '@nestjs/common';
import { Backend } from './backend';
import * as Redis from 'ioredis';
import { RedisService } from 'nestjs-redis';
import { ConfigService } from '@nestjs/config';
import * as os from 'os';

@Injectable()
export class BackendService {
  private backend_key = 'nestback-backends';
  private client: Redis.Redis;
  private hostname: string;
  private readonly initialHealthPoints: number;
  private readonly clickDamage: number;

  constructor(
    private redisService: RedisService,
    private configService: ConfigService) {
    this.hostname = os.hostname();
    this.client = this.redisService.getClient();
    this.initialHealthPoints = this.configService.get('initial_health_points');
    this.clickDamage = this.configService.get('click_damage');
    this.initBackend(this.hostname);
  }

  async initBackend(backend: string): Promise<any> {
    this.client.hset(this.backend_key, backend, this.initialHealthPoints);
  }

  async shootBackend(backend: string) {
    await this.client.hincrby(this.backend_key, backend, this.clickDamage * -1);
  }

  async getBackend(backendName: string): Promise<Backend>{
    const backend = new Backend();
    await this.client.hget(this.backend_key, backendName).then(result => {
      backend.name = backendName;
      backend.health = parseInt(result, 10);
    });
    return backend;
  }

  async getBackends(): Promise<Backend[]> {
    const backends = new Array<Backend>();
    await this.client.hgetall(this.backend_key).then(results => {
      Object.keys(results).forEach(key => {
        backends.push({
          name: key,
          health: parseInt(results[key], 10),
        });
      });
    });
    return backends;
  }
}
