import { Injectable } from '@nestjs/common';
import { HealthCheckError, HealthCheckService, HealthIndicator, HealthIndicatorResult } from '@nestjs/terminus';
import { BackendService } from 'src/backend.service';
import { Backend } from '../backend';

@Injectable()
export class BackendHealthIndicator extends HealthIndicator{
  constructor(private backendService: BackendService) {
    super();
  }
  async isHealthy(key: string): Promise<HealthIndicatorResult>{
    const backend: Backend = await this.backendService.getBackend(key);
    const isHealthy = backend.health > 0;
    const result = this.getStatus(key, isHealthy)
    if (isHealthy){
      return result;
    }
    throw new HealthCheckError('Backend failed', result);
  }
}