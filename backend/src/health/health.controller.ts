import { Controller, Get } from '@nestjs/common';
import { HealthCheck, HealthCheckService } from '@nestjs/terminus';
import { BackendHealthIndicator } from './backend.health';
import * as os from 'os';

@Controller('health')
export class HealthController {
  constructor(
    private health: HealthCheckService,
    private backend: BackendHealthIndicator,
  ) {}

  @Get()
  @HealthCheck()
  check(){
    return this.health.check([
      () => this.backend.isHealthy(os.hostname()),
    ]);
  }
}
