import { Request, Response } from 'express';
import { Injectable, Logger, NestMiddleware } from '@nestjs/common';
import * as os from 'os';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  private logger = new Logger('HTTP');

  // eslint-disable-next-line @typescript-eslint/ban-types
  use(request: Request, response: Response, next: Function): void {
    const { ip, method, originalUrl: url } = request;
    const hostname = os.hostname();
    const userAgent = request.get('user-agent') || '';
    const referer = request.get('referer') || '';

    response.on('close', () => {
      const { statusCode, statusMessage } = response;
      const contentLength = response.get('content-length');
      this.logger.debug(`[${hostname}] "${method} ${url}" ${statusCode} ${statusMessage} ${contentLength} "${referer}" "${userAgent}" "${ip}"`);
    });

    next();
  }
}