import React from 'react';
import Button from '@material-ui/core/Button';
import { AxiosResponse } from 'axios';

const axios = require('axios').default;


class BackendButton extends React.Component<any, any> {
  constructor(props: React.ReactPropTypes) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    axios.post(process.env.REACT_APP_BACKEND_URL, { name: this.props.name });
    this.props.game.getBackends();
  }

  render() {
    if (this.props.health <= 0){
      return <Button key={this.props.name}
                     variant="contained"
                     disabled>
        {this.props.name}<br/>{this.props.health}
      </Button>;
    }
    return <Button key={this.props.name}
                   variant="contained"
                   color="primary"
                   onClick={() => this.handleClick()}>
      {this.props.name}<br/>{this.props.health}
    </Button>;

  }
}


class Game extends React.Component<any, any> {
  constructor(props: React.ReactPropTypes) {
    super(props);
    this.state = { backends: [] };
  }

  componentDidMount() {
    this.getBackends();
  }

  getBackends() {
    axios.get(process.env.REACT_APP_BACKEND_URL).then((response: AxiosResponse) => {
      this.setState({ backends: response.data.backends });
    });
  }

  render() {
    return (
      <form>{this.state.backends.map((backend: any) => (
      <BackendButton key={backend.name} name={backend.name} health={backend.health} game={this} />
    ))}
      </form>);
  }
}

function App() {
  return <Game />;
}

export default App;
